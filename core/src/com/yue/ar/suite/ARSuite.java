package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class ARSuite extends ApplicationAdapter {
	SpriteBatch batch;

	private Stage stage;
	
	@Override
	public void create () {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

		MyActor myActor = new MyActor();

		MoveToAction moveAction = new MoveToAction();
		RotateToAction rotateAction = new RotateToAction();
		ScaleToAction scaleAction = new ScaleToAction();
		SequenceAction sequenceAction = new SequenceAction();

		moveAction.setPosition(300f, 300f);
		moveAction.setDuration(5f);
		rotateAction.setRotation(90f);
		rotateAction.setDuration(5f);
		scaleAction.setScale(0.5f);
		scaleAction.setDuration(5f);

		sequenceAction.addAction(moveAction);
		sequenceAction.addAction(rotateAction);
		sequenceAction.addAction(scaleAction);

		myActor.addAction(scaleAction);
		stage.addActor(myActor);

		stage.addActor(myActor);


	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	@Override
	public void dispose () {

		stage.dispose();

	}
}
